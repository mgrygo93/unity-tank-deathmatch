﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Panda;

public class AIMove : MonoBehaviour
{
    public float safeDistance = 15.0f;
    public float safeHPAmount = 30.0f;
    public Transform[] safeSpots;

    private NavMeshAgent agent;
    private HP hp;
    private int lastSafeSpotIndex = -1;


    private void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        hp = this.GetComponent<HP>();
    }

    [Task]
    public bool InDanger()
    {
        bool safeFromOtherTanks = true;

        for (int i = 0; i < TanksAccess.Instance.tanks.Count; i++)
            if (TanksAccess.Instance.tanks[i].name != gameObject.name)
            {
                if (Vector3.Distance(transform.position, TanksAccess.Instance.tanks[i].transform.position) < safeDistance)
                {
                    safeFromOtherTanks = false;
                    break;
                }
            }
        
        //tank is in danger when it's close to other tanks and have less less than safe amount of HP
        return (!safeFromOtherTanks && hp.healthPoints < safeHPAmount);
    }

    [Task]
    public void Flee()
    {
        int safeSpotIndex = 0;
        do safeSpotIndex = Random.Range(0, safeSpots.Length);
        while (safeSpotIndex == lastSafeSpotIndex);
        lastSafeSpotIndex = safeSpotIndex;
        agent.SetDestination(safeSpots[safeSpotIndex].position);
        Task.current.Succeed();
    }

    [Task]
    public void MoveToDestination()
    {
        if (agent.remainingDistance <= agent.stoppingDistance && !agent.pathPending) Task.current.Succeed();
    }
}
