﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Panda;

public class AIAttack : Shoot
{
    public float attackAngle = 10.0f;
    public float attackRange = 5.0f;

    private NavMeshAgent agent;
    private GameObject target;    

    private void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
    }

    [Task]
    public bool PickEnemy() //picks closest enemy
    {
        bool targetAcquired = false;
        int closestIndex = 0;
        float closestDistance = 1000.0f;

        for(int i = 0; i < TanksAccess.Instance.tanks.Count; i++)
            if(TanksAccess.Instance.tanks[i].name != gameObject.name)
            {
                targetAcquired = true; //different tank exists
                float distance = Vector3.Distance(transform.position, TanksAccess.Instance.tanks[i].transform.position);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestIndex = i;
                }
            }

        target = TanksAccess.Instance.tanks[closestIndex];
        if (target == null) return false;
        return targetAcquired;
    }

    [Task]
    public bool EnemyInAttackRange()
    {
        if (target == null) return false;
        return (Vector3.Distance(transform.position, target.transform.position) < attackRange);
    } 

    [Task]
    public void LookAtTarget()
    {
        //rotate the tank towards its target
        Vector3 direction = target.transform.position - this.transform.position;
        this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, Quaternion.LookRotation(direction), 1.0f);

        if (Task.isInspected)
            Task.current.debugInfo = string.Format("angle={0}",
                Vector3.Angle(this.transform.forward, direction));

        if (Vector3.Angle(this.transform.forward, direction) < attackAngle) Task.current.Succeed();
    }

    [Task]
    public bool ShootAtEnemy()
    {
        Fire();
        return true;
    }

    [Task]
    public void ChaseTarget()
    {
        if(target == null) Task.current.Fail();
        else
        {
            agent.SetDestination(target.transform.position);
            if (agent.remainingDistance <= attackRange && !agent.pathPending)
            {
                agent.ResetPath();
                Task.current.Succeed();
            }
        }
    }
}
