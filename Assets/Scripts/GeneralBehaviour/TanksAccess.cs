﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TanksAccess : MonoBehaviour
{
    private static TanksAccess _instance;
    public static TanksAccess Instance { get { return _instance; } }

    public List<GameObject> tanks = new List<GameObject>();

    private bool gameWon = false;

    private void Awake()
    {
        //implementing singleton
        if (_instance != null && _instance != this) Destroy(this.gameObject);
        else _instance = this;

        //getting tanks data
        GameObject[] allTanks = GameObject.FindGameObjectsWithTag("Tank");
        foreach (GameObject gm in allTanks)
            tanks.Add(gm);
    }

    private void Update()
    {
        //write win message if only player survived
        if(tanks.Count == 1 && tanks[0].name.Equals("Player") && !gameWon)
        {
            ReloadScene.Instance.SetReloadText(ReloadText.Win);
            ReloadScene.Instance.reloadInfoPanel.SetActive(true);
            gameWon = true;
        }
    }

    public void RemoveTank(string tankName)
    {
        for(int i = 0; i < tanks.Count; i++)
            if(tanks[i].name.Equals(tankName))
            {
                tanks.RemoveAt(i);
                break;
            }
    }    
}