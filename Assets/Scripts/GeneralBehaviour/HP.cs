﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HP : MonoBehaviour
{
    public float healthPoints = 100f;
    public GameObject deathEffect;

    internal float maxHealthPoints;
    private bool isDying = false;
    private UpdateUI uiUpdate;
    
    public AudioClip deathExplosionClip;
    [Range(0.0f, 1.0f)]
    public float deathExplosionVolume = 1.0f;

    private void Awake()
    {
        uiUpdate = this.GetComponent<UpdateUI>();
        maxHealthPoints = healthPoints;
        InvokeRepeating("UpdateHealth", 5, 0.5f);
    }

    private void Update ()
    {
        if (healthPoints <= 0.0f || Input.GetKeyDown(KeyCode.Delete))
            if (!isDying)
            {
                isDying = true;
                Die();
            }
    }

    void UpdateHealth()
    {
        if (healthPoints < 100)
        {
            uiUpdate.UpdateHealthBar();
            healthPoints++;
        }
            
    }

    private void Die()
    {
        //play proper audio
        AudioManager.Instance.PlaySound(deathExplosionClip, deathExplosionVolume);

        //instantiate death particle effect
        GameObject destroyEffect = Instantiate(deathEffect);
        destroyEffect.transform.position = this.transform.position;
        ParticleSystem ps = destroyEffect.GetComponent<ParticleSystem>();
        float totalDuration = ps.duration + ps.startLifetime;

        //remove tank from the list
        TanksAccess.Instance.RemoveTank(gameObject.name);

        //if player dies, write 'game over' message
        if (gameObject.name.Equals("Player"))
        {
            ReloadScene.Instance.SetReloadText(ReloadText.Lose);
            ReloadScene.Instance.reloadInfoPanel.SetActive(true);
        }

        //destroy unneeded game objects
        Destroy(destroyEffect, totalDuration);
        Destroy(gameObject);
    }
}
