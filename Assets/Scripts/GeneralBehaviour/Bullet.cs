﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	public GameObject explosion;
    public float damage = 15.0f;

    public AudioClip shootClip;
    [Range(0.0f, 1.0f)]
    public float shootVolume = 0.5f;

    private void Awake()
    {
        AudioManager.Instance.PlaySound(shootClip, shootVolume);
    }

    private void OnCollisionEnter(Collision col)
    {
        bool anotherBulletHit = col.gameObject.tag.Equals("Bullet");
        if (anotherBulletHit) Physics.IgnoreCollision(col.collider, this.GetComponent<Collider>());

        if (col.gameObject.tag.Equals("Tank"))
        {
            col.gameObject.GetComponent<HP>().healthPoints -= damage;
            col.gameObject.GetComponent<UpdateUI>().UpdateHealthBar();
        }

        if(!anotherBulletHit)
        {
            GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
            Destroy(e, 1.5f);
            Destroy(this.gameObject);
        }
    }
}
