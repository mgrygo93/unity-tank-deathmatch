﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject bullet;
    public GameObject turret;
    public float shootForce = 1000.0f;
    
    protected void Fire()
    {
        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        Physics.IgnoreCollision(b.GetComponent<Collider>(), GetComponent<Collider>());
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * shootForce);
    }
}
