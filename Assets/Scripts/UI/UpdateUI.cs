﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateUI : MonoBehaviour
{    
    public GameObject playerHealthbarPrefab;    
    public Transform healthbarPosition;

    protected GameObject canvas;
    protected GameObject playerHealthbarObject;

    protected Image healthBar;
    protected HP hp;    

    protected virtual void Start()
    {
        hp = this.gameObject.GetComponent<HP>();
        canvas = GameObject.FindWithTag("MainCanvas");

        //instantiate player health bar
        playerHealthbarObject = Instantiate(playerHealthbarPrefab, Vector3.zero, Quaternion.identity);
        playerHealthbarObject.transform.SetParent(canvas.transform);
        playerHealthbarObject.transform.localScale = Vector3.one;

        healthBar = playerHealthbarObject.transform.Find("HPLeft").GetComponent<Image>();
    }

    protected virtual void LateUpdate()
    {
        //update healthbar position
        Vector3 hpBarPosition = Camera.main.WorldToScreenPoint(healthbarPosition.position);
        playerHealthbarObject.transform.position = hpBarPosition;
    }

    protected virtual void OnDestroy()
    {
        Destroy(playerHealthbarObject);
    }

    public void UpdateHealthBar ()
    {
        healthBar.fillAmount = Mathf.Clamp(hp.healthPoints / hp.maxHealthPoints, 0.0f, 1.0f);
    }
}
