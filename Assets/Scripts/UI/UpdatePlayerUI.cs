﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdatePlayerUI : UpdateUI
{
    public GameObject playerNamePrefab;
    public Transform playerNamePosition;
    
    private GameObject playerNameObject;
    
    protected override void Start ()
    {
        base.Start();

        //instantiate player name label
        playerNameObject = Instantiate(playerNamePrefab, Vector3.zero, Quaternion.identity);
        playerNameObject.transform.SetParent(canvas.transform);
        playerNameObject.transform.localScale = Vector3.one;
    }
    
    protected override void LateUpdate()
    {
        base.LateUpdate();

        //update player name position
        Vector3 namePosition = Camera.main.WorldToScreenPoint(playerNamePosition.position);
        playerNameObject.transform.position = namePosition;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        Destroy(playerNameObject);
    }
}
