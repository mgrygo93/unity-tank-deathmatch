﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum ReloadText
{
    Lose,
    Win
}

public class ReloadScene : MonoBehaviour
{
    private static ReloadScene _instance;
    public static ReloadScene Instance { get { return _instance; } }

    public GameObject reloadInfoPanel;
    public Text reloadInfoText;

    private void Awake()
    {
        //implementing singleton
        if (_instance != null && _instance != this) Destroy(this.gameObject);
        else _instance = this;
    }

    private void Update ()
    {
        if (reloadInfoPanel.activeSelf && Input.GetKeyDown(KeyCode.Tab)) SceneManager.LoadScene("MainScene");
	}

    public void SetReloadText(ReloadText rt)
    {
        switch(rt)
        {
            case ReloadText.Lose:
                reloadInfoText.text = "Game Over! \nPress 'Tab' to reload";
                break;
            case ReloadText.Win:
                reloadInfoText.text = "You win! \nPress 'Tab' to reload";
                break;
            default:
                reloadInfoText.text = "";
                break;
        }
    }
}
