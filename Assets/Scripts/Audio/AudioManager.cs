﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static AudioManager _instance;
    public static AudioManager Instance { get { return _instance; } }

    private void Awake()
    {
        //implementing singleton
        if (_instance != null && _instance != this) Destroy(this.gameObject);
        else _instance = this;
    }

    public void PlaySound(AudioClip clip, float volume)
    {
        if (clip != null)
        {
            AudioSource audioSource = this.gameObject.AddComponent<AudioSource>();
            audioSource.clip = clip;
            float duration = clip.length;
            audioSource.volume = Mathf.Clamp(volume, 0.0f, 1.0f);
            audioSource.Play();
            Destroy(audioSource, duration);
        }
        else print("AudioClip is null!");
    }
}
