﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DriveTank : MonoBehaviour
{
 	public float speed = 10.0F;
    public float rotationSpeed = 100.0F;

    private Rigidbody rb;

    private void Start()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    private void LateUpdate()
    {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        Quaternion turn = Quaternion.Euler(0.0f, rotation, 0.0f);
        rb.MovePosition(rb.position + this.transform.forward * translation);
        rb.MoveRotation(rb.rotation * turn);
    }
}
